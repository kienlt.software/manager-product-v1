package com.example.shop.controller;

import com.example.shop.exception.ResourceNotFoundException;
import com.example.shop.models.Product;
import com.example.shop.models.ResponseObject;
import com.example.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class ProductController {
    @Autowired
    private ProductService productService;

    //Get all products
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProduct() {
        List<Product> list = productService.getAllProduct();
        return new ResponseEntity<List<Product>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    //Create product
    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        try {
            return new ResponseEntity<Product>(productService.createProduct(product), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<Product>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Upsert
    @PutMapping("/products/{id}")
    public ResponseEntity<Product> createProductOrUpdate(@Valid @RequestBody Product product)
            throws ResourceNotFoundException {
        Product updated = productService.createOrUpdateProduct(product);
        return new ResponseEntity<Product>(updated, new HttpHeaders(), HttpStatus.OK);
    }

    //Get product by specific id
    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") Long id)
            throws ResourceNotFoundException {
        Product product = productService.getProductById(id);
        return new ResponseEntity<Product>(product, new HttpHeaders(), HttpStatus.OK);
    }

    //Delete product by specific id
    @DeleteMapping("/products/{id}")
    public ResponseEntity<ResponseObject> deleteProductById(@PathVariable("id") Long id)
            throws ResourceNotFoundException {
        productService.deleteProductById(id);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("success","Delete product successfully","")
        );
    }
}

