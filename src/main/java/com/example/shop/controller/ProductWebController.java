package com.example.shop.controller;

import com.example.shop.exception.ResourceNotFoundException;
import com.example.shop.models.Product;
import com.example.shop.models.ResponseObject;
import com.example.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("products")
public class ProductWebController {
    @Autowired
    ProductRepository productRepository;

    //Display list of products
    @RequestMapping("")
    public String product(Model model) {
        model.addAttribute("products", productRepository.findAll());
        return "products";
    }

    //Display create product page
    @RequestMapping("/create")
    public String create(Model model) {
        return "createProduct";
    }

    //Create a product
    @PostMapping("/create")
    public <Int> String save(@RequestParam String productName, @RequestParam int year, @RequestParam Double price, @RequestParam String url) {
        Product product = new Product();
        product.setProductName(productName);
        product.setYear(year);
        product.setPrice(price);
        product.setUrl(url);
        productRepository.save(product);
        return "redirect:/products/show/" + product.getId();
    }

    //Display product by specific id
    @RequestMapping("/show/{id}")
    public String show(@PathVariable Long id, Model model) {
        model.addAttribute("product", productRepository.findById(id).get());
        return "show";
    }

    //Delete product by specific id
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id, Model model) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        productRepository.delete(product);
        return "redirect:/products?success";
    }
}
