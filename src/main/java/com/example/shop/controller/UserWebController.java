package com.example.shop.controller;

import com.example.shop.models.User;
import com.example.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("users")
public class UserWebController {
    @Autowired
    UserRepository userRepository;

    //Display list of users
    @RequestMapping("")
    public String product(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "users";
    }

    //Create a user
    @GetMapping("/create")
    public String showCreateForm() {
        return "createProduct";
    }

    //Delete user by specific id
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        userRepository.delete(user);
        return "redirect:/users?success";
    }
}
