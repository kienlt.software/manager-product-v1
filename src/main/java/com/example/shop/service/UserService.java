package com.example.shop.service;

import com.example.shop.models.Product;
import com.example.shop.models.Role;
import com.example.shop.models.User;
import com.example.shop.models.dto.UserRegistrationDto;
import com.example.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    //List all users
    public List<User> getAllUser() {
        List<User> userList = userRepository.findAll();
        if (userList.size() > 0) {
            return userList;
        } else {
            return new ArrayList<User>();
        }
    }


}
