package com.example.shop.service;

import com.example.shop.models.Role;
import com.example.shop.models.User;
import com.example.shop.models.dto.UserRegistrationDto;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserServiceRegistration extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
}
