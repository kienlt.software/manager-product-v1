
<!-- ABOUT THE PROJECT -->
## About The Project
CRUD Objects to manager with login/registration module
- Profile to use admin/admin
* Staging
 ```sh
https://manager-products-staging.herokuapp.com/
 ```
 * Production
 ```sh
https://manager-products-production.herokuapp.com/
 ```
 * Swagger 
 ```sh
https://manager-products-staging.herokuapp.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
 ```
### Built With

* [Spring Boot - JPA - Hibernate](https://spring.io/)
* [Spring Security - Thymeleaf](https://spring.io/)
* [Heroku](https://heroku.com/)
* [MySQL](https://www.mysql.com/)
* [Angular](https://angular.io/) - I'm learning day by day so I will update front-end later.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started
  ```sh
  mkdir manager-product
  ```
  ```sh
  cd manager-products
  ```
  ```sh
  git clone https://gitlab.com/kienlt.software/manager-product-v1.git
  ```
## Coming soon
